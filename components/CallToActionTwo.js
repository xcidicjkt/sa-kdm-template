import React from 'react';

export default class ContactUs extends React.Component{
    render(){
        return(
           <div>
               <section className="mixer-area2 helping-area2">
                <div className="container">
                    <div className="fun-content-wrap">
                        <div>
                            <div className="helping-form">
                                <div className="section-heading">
                                    <div className="section-icon">
                                        <img src="/images/section-icon.png" alt="section-icon" />
                                    </div>
                                    <h2 className="section__title text__white">Contact Us</h2>
                                    <p className="section__meta text__white">Sahabat Anak</p>
                                    <p className="section__meta text__white">Jalan Rawa Dolar no.29, Kampung Raden RT 01 RW 05</p>
                                    <p className="section__meta text__white">Kel. Jatirangon, Kec. Jatisampurna Pondok Gede, Bekasi 17432 Indonesia</p>
                                    <p className="section__meta text__white">Telp: 021-8443545</p>
                                    <p className="section__meta text__white">WA: +62 812-9728-0309</p>

                                </div>
                                <div className="form-shared">
                                    <form action="#">
                                        <div className="row">
                                            <div className="col-lg-8">
                                                <div className="form-group">
                                                    <input type="text" className="form-control"
                                                           placeholder="Name" />
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <div className="form-group">
                                                    <input type="text" className="form-control" placeholder="Purpose" />
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <div className="form-group">
                                                    <input type="email" className="form-control"
                                                           placeholder="Email Address" />
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <div className="form-group">
                                                    <input type="text" className="form-control" placeholder="Phone" />
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <div className="form-group">
                                                    <textarea className="textarea" name="message"
                                                              placeholder="Message"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <button className="theme-btn submit__btn">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
           </div>
        )
    }
}