import React from 'react';
const HelpingArea = () => {
    return (
        <div>
            <section className="mixer-area helping-area">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="section-heading mixer-heading">
                                <div className="section-icon">
                                    <img src="/images/section-icon.png" alt="section-icon" />
                                </div>
                                <h2 className="section__title">Our Works</h2>
                            </div>
                            <div className="helping-item">
                                <div className="row">
                                    <div className="col">
                                        <div className="helping-box helping-box1">
                                            <i className="fa fa-graduation-cap"></i>
                                            {/* <img src="../public/images/Education-icon.png" alt="Education-icon" /> */}

                                            <h4>Education</h4>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="helping-box helping-box2">
                                            <i className="fa fa-calendar"></i>
                                            <h4>Events</h4>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="helping-box helping-box3">
                                            <i className="fa fa-group"></i>
                                            <h4>Advocacy</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="helping-text-box">
                                <p className="helping__text">
                                Its vision is to create an innovative environment to empower street children to become independent and self-support adults. Children come to KDM from ages as young as 7 and from many urban areas of Java and Sumatra.KDM approach covers all areas of a child development, from first contact to basic education to entrepreneurial skills training for young adults.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="mixer-area2 helping-area2">
                <div className="container">
                    <div className="fun-content-wrap">
                        <div>
                            <div className="helping-form">
                                <div className="section-heading">
                                    <div className="section-icon">
                                        <img src="/images/section-icon.png" alt="section-icon" />
                                    </div>
                                    <h2 className="section__title text__white">Make a Donation</h2>
                                    <p className="section__meta text__white">donate us now</p>
                                </div>
                                <div className="form-shared">
                                    <form action="#">
                                        <div className="row">
                                            <div className="col-lg-8">
                                                <div className="form-group">
                                                    <input type="text" className="form-control"
                                                           placeholder="Amount" />
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <div className="form-group">
                                                    <input type="text" className="form-control" placeholder="Full Name" />
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <div className="form-group">
                                                    <input type="email" className="form-control"
                                                           placeholder="Email Address" />
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <div className="form-group">
                                                    <input type="text" className="form-control" placeholder="Location" />
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <div className="form-group">
                                                    <textarea className="textarea" name="message"
                                                              placeholder="Leave a comment"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <button className="theme-btn submit__btn">donate now</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default HelpingArea;
