import React, {Component} from 'react';
import Link from 'next/link';
import LogoSA from '../public/images/js/LogoSA'

class NavOne extends Component {
    constructor(){
        super()
        this.state = {
          sticky: false
        };
    }

    componentDidMount(){
        window.addEventListener('scroll', this.handleScroll);

        //Mobile Menu
        this.mobileMenu();
    }

    handleScroll = () => {

      if (window.scrollY > 100) {
        this.setState({
            sticky: true
        });
      } else if (window.scrollY < 100) {
        this.setState({
            sticky: false
        });
      }

    }

    mobileMenu = () => {
        //Mobile Menu Toggle
        let mainNavToggler = document.querySelector(".mobile-menu-toggle");
        let mainNav = document.querySelector(".side-nav-container");

        mainNavToggler.addEventListener("click", function () {
            mainNav.classList.add('active');
        });

        //Close Mobile Menu
        let closeMenu = document.querySelector(".side-menu-close");
            closeMenu.addEventListener("click", function () {
            mainNav.classList.remove('active');
        });

    }

    render() {
        return (
            <div>
                <header className="header-area">
                    <div className="header-top-action">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-5">
                                    <div className="top-action-content">
                                        <div className="info-box info-box-1 d-flex align-items-center">
                                            <ul className="d-flex align-items-center">
                                                <li><a href="#"><i
                                                    className="fa fa-envelope"></i>admin@kdm.or.id</a></li>
                                                <li><a href="#"><i className="fa fa-phone-square"></i>021-8443545</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-7">
                                    <div className="top-action-content info-action-content">
                                        <div className="info-box info-box-2 d-flex align-items-center justify-content-end">
                                            <ul className="top-action-list d-flex align-items-center">
                                                
                                                
                                                <li><img src="/images/flag-id.png" /></li>
                                                <li><img src="/images/flag-en.png" /></li>
                                                <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i className="fa fa-pinterest"></i></a></li>
                                                <li><a href="#"><i className="fa fa-instagram"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={`header-top header-menu-action ${this.state.sticky ? 'header-fixed' : ''}`}>
                        <div className="container">
                            <div className="row ostion-top-wrap">
                                <div className="col-lg-3 col-sm-5 site-branding">
                                    <div className="logo-action d-flex align-items-center">
                                        <div className="ostion-logo">
                                            <Link href="/">
                                                <a>
                                                    {/* <img src="https://sahabatanak.org/static/logoSAWhite.png" alt="Oxpitan" title="Oxpitan" /> */}
                                                    <LogoSA width="140"/>
                                                </a>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-sm-7 ostion-menu">
                                    <div className="ostion-menu-innner">
                                        <div className="ostion-menu-content">
                                            <div className="navigation-top">
                                                <nav className="main-navigation">
                                                    <ul>
                                                        <li className="active"><Link href="/"><a>Home</a></Link>
                                                        </li>
                                                        <li><a href="#">About Us</a>
                                                            <ul className="dropdown-menu-item">
                                                                <li><Link href="/"><a>About Organization</a></Link></li>
                                                                <li><Link href="/"><a>Vision</a></Link></li>
                                                                <li><Link href="/"><a>Mission statement</a></Link></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="#">Get Involved</a>
                                                            <ul className="dropdown-menu-item">
                                                                <li><Link href="/"><a>Donation</a></Link></li>
                                                                <li><Link href="/"><a>Volunteer</a></Link></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="#">Gallery</a>
                                                        </li>
                                                        {/* <li><a href="#">Article</a>
                                                        </li> */}
                                                        <li><Link href="/"><a>Blog</a></Link></li>
                                                        <li><Link href="/"><a>Event</a></Link></li>
                                                        <li><Link href="/"><a>Contact Us</a></Link></li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                        <div className="mobile-menu-toggle">
                                            <i className="fa fa-bars"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="side-nav-container">
                        <div className="humburger-menu">
                            <div className="humburger-menu-lines side-menu-close"></div>
                        </div>
                        <div className="side-menu-wrap">
                            <ul className="side-menu-ul">
                                <li className="sidenav__item"><a href="/">Home</a>
                                    {/* <span className="menu-plus-icon"></span> */}
                                    {/* <ul className="side-sub-menu">
                                        <li><Link href="/"><a>Home 1</a></Link></li>
                                        <li><Link href="/index2"><a>Home 2</a></Link></li>
                                    </ul> */}
                                </li>
                                <li className="sidenav__item"><a href="#">About Us</a>
                                    <span className="menu-plus-icon"></span>
                                    <ul className="side-sub-menu">
                                        <li><Link href="/"><a>About Organization</a></Link></li>
                                        <li><Link href="/"><a>Vision</a></Link></li>
                                        <li><Link href="/"><a>Mission statement</a></Link></li>
                                    </ul>
                                </li>
                                <li className="sidenav__item"><a href="#">Get Involved</a>
                                    <span className="menu-plus-icon"></span>
                                    <ul className="side-sub-menu">
                                        <li><Link href="/"><a>Donation</a></Link></li>
                                        <li><Link href="/"><a>Volunteer</a></Link></li>
                                    </ul>
                                </li>
                                <li className="sidenav__item"><a href="#">Gallery</a>
                                    {/* <span className="menu-plus-icon"></span> */}
                                    {/* <ul className="side-sub-menu">
                                        <li><Link href="/news"><a>news</a></Link></li>
                                        <li><Link href="/single-news"><a>news detail</a></Link></li>
                                    </ul> */}
                                </li>
                                <li className="sidenav__item"><a href="#">Article</a>
                                    {/* <span className="menu-plus-icon"></span>
                                    <ul className="side-sub-menu">
                                        <li><Link href="/about"><a>about</a></Link></li>
                                        <li><Link href="/gallery"><a>gallery</a></Link></li>
                                        <li><Link href="/volunteer"><a>become a volunteer</a></Link></li>
                                        <li><Link href="/team"><a>our team</a></Link></li>
                                        <li><Link href="/sponsor"><a>sponsors</a></Link></li>
                                    </ul> */}
                                </li>
                                <li className="sidenav__item"><Link href="/"><a>Blog</a></Link></li>
                                <li className="sidenav__item"><Link href="/"><a>Event</a></Link></li>
                                <li className="sidenav__item"><Link href="/"><a>Contact Us</a></Link></li>


                            </ul>
                            <ul className="side-social">
                                <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i className="fa fa-youtube-play"></i></a></li>
                                <li><a href="#"><i className="fa fa-google-plus"></i></a></li>
                            </ul>
                            <div className="side-btn">
                                <Link href="/donate"><a className="theme-btn">donate now</a></Link>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        );
    }
}

export default NavOne;