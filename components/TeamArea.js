import React from 'react';

const TeamArea = () => {
    return (
        <section className="team-area text-center">
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 mx-auto">
                        <div className="section-heading">
                            <div className="section-icon">
                                <img src="/images/section-icon.png" alt="section-icon" />
                            </div>
                            <h2 className="section__title">Gallery</h2>
                            {/* <p className="section__meta">meet profesionals</p> */}
                        </div>
                    </div>
                </div>
                <div className="row team-content-wrap">
                    <div className="col-lg-3 col-sm-6">
                        <div className="team-item team-item1">
                            <div className="team__img">
                                <img src="https://storage.googleapis.com/project-sahabat-anak.appspot.com/public/attachments/1584587778791-1524024413441-05-sahabat-anak-min-jpg-origin-2-jpeg-large" alt="team image" />
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6">
                        <div className="team-item team-item1">
                            <div className="team__img">
                                <img src="https://storage.googleapis.com/project-sahabat-anak.appspot.com/public/attachments/1584587778791-1524024413441-05-sahabat-anak-min-jpg-origin-2-jpeg-large" alt="team image" />
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6">
                        <div className="team-item team-item1">
                            <div className="team__img">
                                <img src="https://storage.googleapis.com/project-sahabat-anak.appspot.com/public/attachments/1584587778791-1524024413441-05-sahabat-anak-min-jpg-origin-2-jpeg-large" alt="team image" />
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6">
                        <div className="team-item team-item1">
                            <div className="team__img">
                                <img src="https://storage.googleapis.com/project-sahabat-anak.appspot.com/public/attachments/1584587778791-1524024413441-05-sahabat-anak-min-jpg-origin-2-jpeg-large" alt="team image" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default TeamArea;
