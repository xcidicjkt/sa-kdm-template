import React, {Component} from 'react';
import CountUp from 'react-countup';
import VisibilitySensor from 'react-visibility-sensor';
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css';

class MixerArea extends Component {
    constructor(){
        super()
        this.state = {
            startCounter: false
        }
    }

    onVisibilityChange = isVisible => {
        if (isVisible) {
            this.setState({startCounter: true});
        }
    }


    render() {
        return (
            <div>
                <section className="mixer-area">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8">
                                <div className="section-heading mixer-heading">
                                    <div className="section-icon">
                                        <img src="/images/section-icon.png" alt="section-icon" />
                                    </div>
                                    <h2 className="section__title text__white">Who We Help</h2>
                                    {/* <a href="/donate" className="theme-btn">start donation</a> */}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </section>
                <section className="mixer-area2">
                    <div className="container">
                    <div className="row service-wrap">
                        <div className="col">
                            <div className="service-item service-item1 mixer-area2-margin">
                                <div className="service-item-inner">
                                    {/* <div className="service-icon">
                                        <i className="icon-peace-1"></i>
                                    </div> */}
                                    <div className="service-content">
                                        <h4 className="service__title">Children of the Street</h4>
                                        <p className="service__desc">
                                        Children who spend 100% of their time on the street: eating, sleeping, working and living there. They do not go to school, they do not have a family and a place to stay. Frequently, they have run away from their families and never return.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="service-item service-item2">
                                <div className="service-item-inner">
                                    {/* <div className="service-icon">
                                        <i className="icon-praying"></i>
                                    </div> */}
                                    <div className="service-content">
                                        <h4 className="service__title">Children on the Street</h4>
                                        <p className="service__desc">
                                        Children who are forced to work on the street to help their families. This category of street children frequently goes to school and still lives with their family.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="service-item service-item3">
                                <div className="service-item-inner">
                                    {/* <div className="service-icon">
                                        <i className="icon-peace"></i>
                                    </div> */}
                                    <div className="service-content">
                                        <h4 className="service__title">Children Vulnerable to be on the Street</h4>
                                        <p className="service__desc">
                                        These children mix with those from the 1st and 2nd categories and are vulnerable to end up living on the streets because of domestic abuse, desire to earn their own money or a desire to live freely.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                </section>
               
            </div>
        );
    }
}

export default MixerArea;