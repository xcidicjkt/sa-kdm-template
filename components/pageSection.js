import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import PageStyle from 'styles/web/page';
import convert from 'htmr';

const styleSheet = () => ({
  root: {
    flexDirection: 'column',
    width: '100%',
    display: 'flex',
    padding: '0 0 20px',
    boxSizing: 'border-box',
    minHeight: 'calc(100vh - 150px)',
    position: 'relative',
    '& > div': {
      padding: 0,
      boxSizing: 'border-box',
      overflow: 'hidden',
    },
    '& > div:first-child': {
      padding: '20px',
    },
  },
  page: PageStyle,
  content: {
    flex: 1,
    order: 2,
  },
  images: {
    flex: 1,
    order: 1,
    '& img': {
      width: '100%',
      height: 'auto',
      margin: 0,
      padding: 0,
    },
  },
  '@media (min-width: 550px)': {
    root: {
      padding: '50px 30px 30px',
      '& > div': {
        padding: 10,
      },
      '& > div:first-child': {
        padding: 'inherit',
        paddingTop: 0,
      },
    },
    images: {
      padding: '0 16px',
      margin: '12px 0 10px',
    },
  },
  '@media (min-width: 750px)': {
    root: {
      flexDirection: 'row',
    },
    content: {
      order: 1,
      marginRight: '2%',
    },
    images: {
      marginLeft: '2%',
      order: 2,
      margin: '16px 0 30px',
    },
  },
  float: {
    '@media (min-width: 750px)': {
      position: 'fixed',
      maxWidth: '39%',
      top: 208,
      width: '100%',
    },
    '@media (min-width: 1100px)': {
      maxWidth: '40%',
    },
    '@media (min-width: 1280px)': {
      maxWidth: '41%',
    },
  },
});
class PageSection extends React.Component {
  state = {
    float: false,
  };
  componentDidMount() {
    if (window) {
      window.addEventListener('scroll', this.handleScroll);
    }
  }

  componentWillUnmount() {
    if (window) {
      window.removeEventListener('scroll', this.handleScroll);
    }
  }
  handleMobile = (e) => {
    this.setState({ mobile: e });
  };
  handleScroll = () => {
    const scroll = window.pageYOffset || document.documentElement.scrollTop;
    if (scroll > 150) {
      this.setState({ float: true });
    } else {
      this.setState({ float: false });
    }
  };
  render() {
    const {
      classes,
      content,
      order,
      title,
      leftContent,
      children,
      rightContent,
      floatingImage,
    } = this.props;
    return (
      <div className={classes.root}>
        <div style={{ order: order === 'revert' ? '1' : '2' }} className={`${classes.page} ${classes.content}`}>
          <h2>{title}</h2>
          {content && convert(content)}
          {children || leftContent || null}
        </div>
        <div className={classes.images}>
          {this.props.img && (
            <img
              ref={(n) => {
                this.image = n;
              }}
              src={this.props.img}
              className={floatingImage ? classes.float : ''}
              alt={`KDM ${title}`}
              title={title}
            />
          )}
          {rightContent && rightContent}
        </div>
      </div>
    );
  }
}

PageSection.defaultProps = {
  floatingImage: true,
};

PageSection.propTypes = {
  classes: PropTypes.object.isRequired,
  img: PropTypes.string,
  content: PropTypes.any,
  order: PropTypes.any,
  title: PropTypes.string,
  children: PropTypes.node,
  rightContent: PropTypes.node,
  leftContent: PropTypes.node,
  floatingImage: PropTypes.bool,
};

export default withStyles(styleSheet)(PageSection);
