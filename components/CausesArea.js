import React from 'react';

const CausesArea = () => {
    return (
        <div>
            <section className="causes-area">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 mx-auto">
                            <div className="section-heading blog-heading text-center">
                                <div className="section-icon">
                                    <img src="/images/section-icon.png" alt="section-icon" />
                                </div>
                                <h2 className="section__title">Events</h2>
                                {/* <p className="section__meta">help us now</p> */}
                            </div>
                        </div>
                    </div>
                    <div className="row blog-content-wrap">
                        <div className="col-lg-4">
                            <div className="blog-content">
                                <div className="blog-item blog-item1">
                                    <div className="blog-img">
                                        <img src="https://oxpitan-layerdrops.now.sh/images/img1.jpg" alt="" />
                                    </div>
                                    <div id="bar1" className="barfiller">
                                        <div className="tipWrap">
                                            <span className="tip"></span>
                                        </div>
                                        <span className="fill" data-percentage="23"></span>
                                    </div>
                                    <div className="blog-inner-content">
                                        <h3 className="blog__title"><a href="/causes-detail">Event A</a>
                                        </h3>
                                        <p className="blog__desc">Aliq is notm hendr erit a augue insu image pellen
                                            tes.</p>
                                        <ul className="blog__list">
                                           
                                        </ul>
                                        <a href="/donate" className="theme-btn">join now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4">
                            <div className="blog-content">
                                <div className="blog-item blog-item2">
                                    <div className="blog-img">
                                        <img src="https://oxpitan-layerdrops.now.sh/images/img2.jpg" alt="" />
                                            {/* <span className="blog__tag"><i className="fa fa-flash"></i> trending</span> */}
                                    </div>
                                    <div id="bar2" className="barfiller">
                                        <div className="tipWrap">
                                            <span className="tip"></span>
                                        </div>
                                        <span className="fill" data-percentage="80"></span>
                                    </div>
                                    <div className="blog-inner-content">
                                        <h3 className="blog__title"><a href="/causes-detail">Event B</a>
                                        </h3>
                                        <p className="blog__desc">Aliq is notm hendr erit a augue insu image pellen
                                            tes.</p>
                                        <ul className="blog__list">
                                            
                                        </ul>
                                        <a href="/donate" className="theme-btn">join now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4">
                            <div className="blog-content">
                                <div className="blog-item blog-item3">
                                    <div className="blog-img">
                                        <img src="https://oxpitan-layerdrops.now.sh/images/img3.jpg" alt="" />
                                    </div>
                                    <div id="bar3" className="barfiller">
                                        <div className="tipWrap">
                                            <span className="tip"></span>
                                        </div>
                                        <span className="fill" data-percentage="44"></span>
                                    </div>
                                    <div className="blog-inner-content">
                                        <h3 className="blog__title"><a href="/causes-detail">Event C</a></h3>
                                        <p className="blog__desc">Aliq is notm hendr erit a augue insu image pellen
                                            tes.</p>
                                        <ul className="blog__list">
                                           
                                        </ul>
                                        <a href="/donate" className="theme-btn">join now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-8 mx-auto">
                            <div className="section-heading blog-heading text-center">
                                <h2 className="section__title">Our Partners and Sponsors</h2>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    );
};

export default CausesArea;
