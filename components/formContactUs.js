import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withReduxSaga } from 'root/store';
import {
  selectLoading,
  selectError,
  selectContactUs,
} from 'selectors/publicSelector';
import compose from 'recompose/compose';
import { createStructuredSelector } from 'reselect';
import Formsy, { addValidationRule } from 'formsy-react';
import Snackbar from 'components/snackbar';

import { withStyles } from 'material-ui/styles';
import style from 'styles/web/styles';
import TextInput from './inputform/textInputMaterial';
import TextArea from './inputform/textAreaMaterial';
import Button from './button/saButton';
// import { addContactUsPublics } from 'actions/publicAction';
import configRecaptcha from 'utils/recaptcha';

const Recaptcha = require('react-recaptcha');

// generalValidation
addValidationRule('isTextField', (obj, value) => {
  if (value) {
    return value.length < 100;
  }
});
let helper = 'This field is required!';

// Inline styling of JSX
const styles = () => ({
  root: {
    width: '100%',
    paddingLeft: '0rem',
    boxSizing: 'border-box',
    fontFamily: 'Roboto, Arial',
    '& section': {
      animation: 'animfade 0.5s ease-in-out',
    },
    '@media (min-width: 550px)': {
      paddingLeft: '3rem',
    },
  },
  '@keyframes animfade': style.formAnimation,
  titleStep: {
    textTransform: 'uppercase',
    fontSize: '0.875em',
    color: '#3C70A6',
    letterSpacing: '1px',
    display: 'inline-block',
    position: 'relative',
    marginBottom: 20,
    '&:after': {
      content: '""',
      height: 2,
      width: '100%',
      position: 'absolute',
      background: '#3C70A6',
      left: 0,
      bottom: -4,
    },
  },
  formsy: {
    marginTop: 20,
  },
  subTitle: {
    margin: '20px 0 10px',
    padding: 0,
    fontSize: '0.875em',
    display: 'block',
    fontStyle: 'italic',
  },
  underline: { textDecoration: 'underline' },
  total: {
    paddingLeft: '2em',
    display: 'inline-block',
    boxSizing: 'border-box',
    width: '100%',
    '& span:first-child': {
      float: 'left',
    },
    '& span:last-child': {
      float: 'right',
      color: '#3C70A6',
    },
  },
  buttonAction: {
    display: 'flex',
    width: '100%',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    '& > div': {
      width: 'auto',
    },
  },
});

// Stepper page
const Step = (props) => (
  <section style={{ display: 'block', width: '100%' }}>
    {props.children}
  </section>
);
Step.propTypes = { children: PropTypes.any };

// title
const SubTitle = (props) => (
  <span onClick={props.onClick} className={props.class}>
    {props.text}
  </span>
);
SubTitle.propTypes = {
  text: PropTypes.any,
  class: PropTypes.any,
  onClick: PropTypes.any,
};

// Volunter form components
class FormContactUs extends React.Component {
  state = {
    clientLoad: false,
    openAlert: false,
    canSubmit: false,
    fullNameValue: '',
    addressValue: '',
    phoneValue: '',
    emailValue: '',
    purposeValue: '',
    messageValue: '',
    loading: false,
    textAlert: '',
    step: 1,
  };
  componentWillMount() {
    if (this.props.lang !== 'en') {
      helper = 'Field ini wajib di isi!';
    }
  }
  componentDidMount() {
    this.getClientLoad();
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.dataContact &&
      this.props.dataContact !== nextProps.dataContact
    ) {
      this.setState({ step: 0 });
      this.resetForm();
    }
    if (nextProps.isError && this.props.isError !== nextProps.isError) {
      this.setState({
        loading: false,
        textAlert: nextProps.isError,
        openAlert: true,
        canSubmit: true,
      });
    }
    if (nextProps.isLoading && this.props.isLoading !== nextProps.isLoading) {
      this.setState({ loading: true, canSubmit: false });
    }
  }
  onloadCallback = () => {
    console.log('captcha render successfully');
  };
  getClientLoad = () => {
    this.setState({ clientLoad: true });
  };
  handleSubmit = async () => {
    const {
      fullNameValue,
      phoneValue,
      emailValue,
      messageValue,
      purposeValue,
    } = this.state;
    const body = await {
      name: fullNameValue,
      purpose: purposeValue,
      phone: phoneValue,
      email: emailValue,
      message: messageValue,
    };
    this.props.dispatch(addContactUsPublics(body));
  };
  handleChangeInput = (field, value) => {
    this.setState({ [field]: value });
  };

  enableButton = () => {
    this.setState({
      canSubmit: true,
    });
    if (this.state.step === 3 && !this.state.approvalValue) {
      this.setState({
        canSubmit: false,
      });
    }
  };
  disableButton = () => {
    this.setState({
      canSubmit: false,
    });
  };

  // manually trigger reCAPTCHA execution
  executeCaptcha = () => {
    this.recaptchaInstance.execute();
  };
  // executed once the captcha has been verified
  // can be used to post forms, redirect, etc.
  verifyCallback = (response) => {
    if (response) {
      this.handleSubmit();
    }
  };
  resetForm = () => {
    this.form.reset();
  };
  // close alert snackbar
  handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ openAlert: false });
  };

  render() {
    const { classes, lang } = this.props;
    const {
      canSubmit,
      clientLoad,
      openAlert,
      textAlert,
      loading,
      step,
    } = this.state;
    return (
      <div className={classes.root}>
        <Formsy
          ref={(c) => {
            this.form = c;
          }}
          onValid={this.enableButton}
          onInvalid={this.disableButton}
          className={classes.formsy}
        >
          {step === 1 && (
            <Step>
              <TextInput
                name="fullname"
                title={lang === 'en' ? 'Name' : 'Nama Lengkap'}
                getInputValue={(value) => {
                  this.handleChangeInput('fullNameValue', value);
                }}
                helper={helper}
                required
                validations="isTextField"
                value={this.state.fullNameValue}
              />
              <TextInput
                name="purpose"
                title={lang === 'en' ? 'Purpose' : 'Keperluan'}
                getInputValue={(value) => {
                  this.handleChangeInput('purposeValue', value);
                }}
                helper={helper}
                required
                validations="isTextField"
                value={this.state.purposeValue}
              />
              <TextInput
                name="email"
                type="email"
                title={lang === 'en' ? 'Email' : 'Alamat Email'}
                getInputValue={(value) => {
                  this.handleChangeInput('emailValue', value);
                }}
                helper={helper}
                validationError="Email is not valid"
                required
                validations="isEmail"
                value={this.state.emailValue}
              />
              <TextInput
                name="phone"
                title={lang === 'en' ? 'Phone' : 'Telepon'}
                type="tel"
                getInputValue={(value) => {
                  this.handleChangeInput('phoneValue', value);
                }}
                validations="isTextField"
                value={this.state.phoneValue}
              />
              <TextArea
                title={lang === 'en' ? 'Message' : 'Pesan'}
                name="message"
                getInputValue={(value) => {
                  this.handleChangeInput('messageValue', value);
                }}
                value={this.state.messageValue}
                validationError="Please explain!"
                helper={helper}
                required
              />
              <div className={classes.buttonAction}>
                <Button
                  onClick={this.executeCaptcha}
                  borderRadius="6px"
                  float="left"
                  buttonName={lang === 'en' ? 'Submit' : 'Submit'}
                  disabled={!canSubmit}
                  background="primary"
                  loading={loading}
                />
              </div>
              {clientLoad && (
                <Recaptcha
                  ref={(e) => (this.recaptchaInstance = e)}
                  verifyCallback={this.verifyCallback}
                  onloadCallback={this.onloadCallback}
                  {...configRecaptcha}
                />
              )}
              <Snackbar
                text={textAlert}
                requestClose={this.handleCloseAlert}
                open={openAlert}
              />
            </Step>
          )}
          {step === 0 && (
            <Step>
              <SubTitle
                class={classes.subTitle}
                text={
                  lang === 'en'
                    ? 'We will look over your message & will be back to you as soon as possible. Thank you.'
                    : 'Kami akan memeriksa pesan anda & akan memberi respon kepada Anda sesegera mungkin. Terima kasih.'
                }
              />
            </Step>
          )}
        </Formsy>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  dataContact: selectContactUs(),
  isLoading: selectLoading(),
  isError: selectError(),
});
FormContactUs.propTypes = {
  classes: PropTypes.object.isRequired,
  lang: PropTypes.string,
  isLoading: PropTypes.any,
  dataContact: PropTypes.any,
  isError: PropTypes.any,
  dispatch: PropTypes.any,
};

export default compose(
  withReduxSaga,
  connect(mapStateToProps),
  withStyles(styles),
)(FormContactUs);
