import React from 'react';

const HiwArea = () => {
    return (
        <section className="hiw-area">
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div className="hiw-item">
                            <div className="hiw-item-img-box hiw-bg1">
                                <figure>
                                    <img src="https://oxpitan-layerdrops.now.sh/images/hiw-img.jpg" alt="" />
                                        <h3 className="hiw-title">Give and share to children in needs</h3>
                                        <div className="hiw-btn-box">
                                            <a href="#" className="theme-btn">Donate Now</a>
                                        </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <div className="hiw-item">
                            <div className="hiw-item-img-box hiw-bg2">
                                <figure>
                                    <img src="https://oxpitan-layerdrops.now.sh/images/hiw-img2.jpg" alt="" />
                                        <h3 className="hiw-title">Be part of our changemaker</h3>
                                        <div className="hiw-btn-box">
                                            <a href="#" className="theme-btn">Join As Volunteer</a>
                                        </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <div className="hiw-item">
                            <div className="hiw-item-img-box hiw-bg3">
                                <figure>
                                    <img src="https://oxpitan-layerdrops.now.sh/images/hiw-img3.jpg" alt="" />
                                        <h3 className="hiw-title">Support Our Product</h3>
                                        <div className="hiw-btn-box">
                                            <a href="#" className="theme-btn">Shop Now</a>
                                        </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default HiwArea;
