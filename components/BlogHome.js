import React from 'react';

const BlogHome = () => {
    return (
        <section className="blog-area">
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 mx-auto text-center">
                        <div className="section-heading blog-heading">
                            <div className="section-icon">
                                <img src="/images/section-icon.png" alt="section-icon" />
                            </div>
                            <h2 className="section__title">Recent Blog Posts</h2>
                            <p className="section__meta">news and updates</p>
                        </div>
                    </div>
                </div>
                <div className="row recent-post-wrap">
                    <div className="col-lg-6">
                        <div className="recent-item">
                            <div className="recent__img">
                                <span className="meta__date-date">09 mar, 2019</span>
                                <img src="https://oxpitan-layerdrops.now.sh/images/blog-img.jpg" alt="service-image" />
                            </div>
                            <div className="news__content">
                                <h3 className="news__content-title"><a href="/single-news">STORY FROM OFFICE: TOUGH DECISION BUT THIS MUST BE TAKEN</a></h3>
                                <ul className="news__content-list">
                                    <li className="news__content-active__dot"><a href="#">mike hardson</a></li>
                                    <li><a href="#">3 comments</a></li>
                                </ul>
                                <p className="news__content-text">
                                Since the opening of the first Covid-19 case in Indonesia, we have begun to think when the situation will get worse.
                                </p>
                                <a href="single-news" className="theme-btn">read more</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="third-recent-box">
                            <ul className="third-recent-item">
                                <li>
                                    <div className="recent__img">
                                        <a href="/single-news"><img src="https://oxpitan-layerdrops.now.sh/images/blog-img2.jpg" alt="" /></a>
                                    </div>
                                    <div className="recent__content">
                                        <span>07 mar, 2019</span>
                                        <h4><a href="single-news.html">FIGHT COVID-19, PROTECT CHILDREN</a>
                                        </h4>
                                    </div>
                                </li>
                                <li>
                                    <div className="recent__img">
                                        <a href="/single-news"><img src="https://oxpitan-layerdrops.now.sh/images/blog-img3.jpg" alt="" /></a>
                                    </div>
                                    <div className="recent__content">
                                        <span>04 mar, 2019</span>
                                        <h4><a href="/single-news">KDM RESPONDS TO COVID-19 - IT'S TIME TO CHANGE!</a></h4>
                                    </div>
                                </li>
                                <li>
                                    <div className="recent__img">
                                        <a href="/single-news"><img src="https://oxpitan-layerdrops.now.sh/images/blog-img4.jpg" alt="" /></a>
                                    </div>
                                    <div className="recent__content">
                                        <span>30 feb, 2019</span>
                                        <h4><a href="single-news.html">NEW SEMESTER, STUDENTS TAKE A NEW ROUTINE</a></h4>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default BlogHome;
