import React, {Component} from 'react';

class Footer extends Component {
    constructor(){
        super()
        this.state = {
          scrollBtn: false
        }
    }

    componentDidMount(){
        window.addEventListener('scroll', this.handleScroll);
    }

    handleScroll = () => {

      if (window.scrollY > 100) {
        this.setState({
            scrollBtn: true
        });
      } else if (window.scrollY < 100) {
        this.setState({
            scrollBtn: false
        });
      }

    }

    scrollTop = () => {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div>
                <section className="footer-area">
                    <div className="newsletter-area">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6 mx-auto text-center">
                            
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="footer-top">
                        <div className="container">
                            <div className="row footer-widget-wrap">
                                <div className="col footer-item footer-item1">
                                    <h3 className="widget__title">About Us</h3>
                                    <ul className="foot__links">
                                        <li><a>About Organization</a></li>
                                        <li><a>Vision</a></li>
                                        <li><a>Mission statement</a></li>
                                       
                                    </ul>
                                </div>
                                <div className="col footer-item footer-item2">
                                    <h3 className="widget__title">Get Involved</h3>
                                    <ul className="foot__links">
                                        <li><a>Donation</a></li>
                                        <li><a>Volunteer</a></li>
                                    </ul>
                                </div>
                                <div className="col footer-item footer-item3">
                                    <h3 className="widget__title">Gallery</h3>
                                </div>
                                {/* <div className="col footer-item footer-item3">
                                    <h3 className="widget__title">Article</h3>
                                </div> */}
                                <div className="col footer-item footer-item3">
                                    <h3 className="widget__title">Blog</h3>
                                </div>
                                <div className="col footer-item footer-item3">
                                    <h3 className="widget__title">Event</h3>
                                </div>
                                {/* <div className="col footer-item footer-item3">
                                    <h3 className="widget__title">Contact Us</h3>
                                </div> */}
                                <div className="col footer-item footer-item4">
                                    <h3 className="widget__title">Contact Us</h3>
                                    <div className="footer__social">
                                        <ul>
                                            <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i className="fa fa-pinterest"></i></a></li>
                                            <li><a href="#"><i className="fa fa-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="footer-copyright">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="copyright-desc">
                                        <p>© Copyright 2020 by <a href="#">PT Xcidic Teknologi Indonesia.</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div onClick={this.scrollTop} id="back-to-top" className={this.state.scrollBtn ? 'back-btn-shown' : ''}>
                    <i className="fa fa-angle-up" title="Go top"></i>
                </div>

            </div>
        );
    }
}

export default Footer;